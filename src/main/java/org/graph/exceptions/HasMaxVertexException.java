package org.graph.exceptions;
/**
 *Ошибка. Невозможно построить граф в котором вершин больше, чем 1000, согласно условию.
 */
public class HasMaxVertexException extends Exception {


    private static final long serialVersionUID = 1L;

    public HasMaxVertexException() {
        super();
    }
}
