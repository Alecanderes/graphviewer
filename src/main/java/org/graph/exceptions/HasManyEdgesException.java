package org.graph.exceptions;

/**
 * Ошибка. Невозможно построить граф в котором ребер большем чем вершин + 1
 */
public class HasManyEdgesException extends Exception {

    private static final long serialVersionUID = 1L;

    public HasManyEdgesException() {
        super();
    }
}
