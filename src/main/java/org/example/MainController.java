package org.example;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.*;
import javafx.scene.control.TextArea;
import org.graph.model.Edge;
import org.graph.model.Graph;
import org.graph.exceptions.HasManyEdgesException;
import org.graph.exceptions.HasMaxVertexException;
import org.graph.model.Vertex;
import java.util.List;

public class MainController {

    @FXML
    TextArea textArea;

    @FXML
    PieChart pieChart;
    @FXML
    BarChart powerChart;

    public MainController() {

    }

    @FXML
    public void menuCreateRandomAction() throws HasMaxVertexException, HasManyEdgesException {
        Graph graph = new Graph(5, 4);
        outGraph(graph);
    }

    @FXML
    public void menuCreateAction() throws HasMaxVertexException, HasManyEdgesException {
        Graph graph = new Graph();
        graph.addEdge("����", "���", 8);
        graph.addEdge("���", "����", 1);
        graph.addEdge("���", "�����", 2);
        graph.addEdge("����", "����", 4);
        graph.addEdge("�����", "����", 2);
        graph.addEdge("�����", "�����", 24);
        //graph.addVertex("����");

        outGraph(graph);


    }

    private void outGraph(Graph graph) {
        String text = "����: " + graph.getVertexList().size() + " ������,  " + graph.getEdgesList().size() + " �����";
        text += "\n\n---����� �����---";
        text += graph.print();

        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("�������");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("�������");
        powerChart.getData().clear();
        powerChart.setTitle("������������� ������ �� ��������");

        text += "\n\n---������������� ������ �� ��������---";
        for (Vertex vertex : graph.getVertexList()) {
            int i = graph.getGradeOfVertex(vertex.getName());
            text += "\n" + vertex + " ������� " + i;
            XYChart.Series<String, Number> series = new XYChart.Series<>();
            series.setName(vertex.toString());
            series.getData().add(new XYChart.Data<>("�������", i));
            powerChart.getData().add(series);
        }

        text += "\n\n---������ ����������� ������������� ���� �� ���������---";
        List<Edge> powerList = graph.getWeightsOfEdges();
        text += "\n" + powerList;


        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
        for (Edge e : powerList) {
            pieChartData.add(new PieChart.Data(e.getName() + " ��� = " + e.getWeight(), e.getWeight()));
        }

        pieChart.setData(pieChartData);
        pieChart.setTitle("������ ����������� ������������� ���� �� ���������");


        text += "\n\n---�������, ������� ���������� ����� ����������� ����---";
        text += "\n" + graph.getMaxAdjacentVertex();

        textArea.setText(text);


    }

}
